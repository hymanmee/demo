package com.hymane.demo.mvp.presenter;

import com.hymane.demo.mvp.base.BasePresenter;
import com.hymane.demo.mvp.contract.ILoginContract;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/13
 * Description:
 */
public class BookPresenterImpl extends BasePresenter<ILoginContract.Model, ILoginContract.View> implements ILoginContract.Presenter {

    protected BookPresenterImpl(ILoginContract.View view) {
        super(view);
    }

    @Override
    public void login(String userId, String password) {
    }
}
