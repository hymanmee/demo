package com.hymane.demo.bean;

import java.io.Serializable;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/13
 * Description:
 */
public class BaseResp<T> implements Serializable {
    public int code;
    public String message;
    public T data;
}
