package com.hymane.demo;

import android.app.Application;
import android.content.Context;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/13
 * Description:hhs
 */
public class App extends Application {
    private static Context app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static Context app() {
        return app;
    }
}
