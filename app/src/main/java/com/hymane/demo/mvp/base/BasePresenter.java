package com.hymane.demo.mvp.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/1
 * Description: 基础Presenter，起业务协调者职责。
 * 处理View层的引用,业务presenter继承本BasePresenter，控制view层引用，
 * 再实现业务presenter方法，处理业务逻辑。
 *
 * @param <M> Model - M 接口
 * @param <V> Model - V 接口
 */
public class BasePresenter<M extends IBaseContract.Model, V extends IBaseContract.View> implements IBaseContract.Presenter {
    //V层弱引用，防止内存泄漏
    private WeakReference<V> mViewRefer;
    @Nullable
    private M mModel;

    /***
     * 自行处理业务逻辑，
     * @param view MVP - V 层
     */
    protected BasePresenter(V view) {
        mViewRefer = new WeakReference<>(view);
    }

    /***
     * 业务逻辑交由 MVP - M 层处理
     * @param model MVP - M 层
     * @param view MVP - V 层
     */
    protected BasePresenter(@NonNull M model, V view) {
        mViewRefer = new WeakReference<>(view);
        mModel = model;
    }

    /***
     * 当前View是否可用，即页面是否还未销毁
     * @return true 可用|false：不可用
     */
    public boolean isViewAttached() {
        return mViewRefer != null && mViewRefer.get() != null;
    }

    /***
     * 获取当前View
     * @return 当前View，或者null(页面已销毁)
     * PS:调用前可使用{@link #isViewAttached()}进行验证
     */
    public V view() {
        if (mViewRefer != null) {
            return mViewRefer.get();
        }
        return null;
    }

    /***
     * 获取当前业务逻辑处理类 - model
     * @return Model，可能为空
     */
    public M model() {
        return mModel;
    }

    /***
     * 业务逻辑处理类是否为空，即逻辑是否交由 model 层处理
     * @return true ： 交由 model 处理；false ：不交
     */
    public boolean isModelAttached() {
        return mModel != null;
    }

    /***
     * 清理View(context)层引用
     * 子类可重写本方法来处理页面结束之后
     * 还未请求或正在请求中当网络请求
     */
    @Override
    public void unsubscribe() {
        if (mViewRefer != null) {
            mViewRefer.clear();
            mViewRefer = null;
        }
    }
}
