package com.hymane.demo.mvp.model;

import com.hymane.demo.api.Server;
import com.hymane.demo.bean.MySetting;
import com.hymane.demo.bean.User;
import com.hymane.demo.mvp.Callback;
import com.hymane.demo.mvp.base.BaseViewImpl;
import com.hymane.demo.mvp.contract.ILoginContract;

import java.util.HashMap;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/12
 * Description:
 */
public class LoginModelImpl implements ILoginContract.Model {
    @Override
    public void login(String userId, String password, Callback callback) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("password", password);
    }

    @Override
    public void saveUser2DB(User user) {
    }

    @Override
    public void fetchSetting(String userId, Callback<MySetting> callback) {
    }
}
