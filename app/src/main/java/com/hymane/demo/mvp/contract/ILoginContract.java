package com.hymane.demo.mvp.contract;

import com.hymane.demo.bean.MySetting;
import com.hymane.demo.bean.User;
import com.hymane.demo.mvp.Callback;
import com.hymane.demo.mvp.base.IBaseContract;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/12
 * Description:
 */
public interface ILoginContract {
    interface Model extends IBaseContract.Model {
        void login(String userId, String password, Callback<User> callback);

        void saveUser2DB(User user);

        void fetchSetting(String userId, Callback<MySetting> callback);
    }

    interface View extends IBaseContract.View {
        void onLogin(User user);
    }

    interface Presenter extends IBaseContract.Presenter {
        void login(String userId, String password);
    }
}
