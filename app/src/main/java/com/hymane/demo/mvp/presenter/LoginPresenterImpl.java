package com.hymane.demo.mvp.presenter;

import com.hymane.demo.bean.User;
import com.hymane.demo.mvp.Callback;
import com.hymane.demo.mvp.base.BasePresenter;
import com.hymane.demo.mvp.contract.ILoginContract;
import com.hymane.demo.mvp.model.LoginModelImpl;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/12
 * Description:
 */
public class LoginPresenterImpl extends BasePresenter<ILoginContract.Model, ILoginContract.View> implements ILoginContract.Presenter {

    public LoginPresenterImpl(ILoginContract.View view) {
        super(new LoginModelImpl(), view);
    }

    @Override
    public void login(final String userId, String password) {
        model().login(userId, password, new Callback<User>() {
            @Override
            public void onSuccess(User data) {
                model().saveUser2DB(data);
//                model().fetchSetting(data.userId)
                if (isViewAttached()) {
                    view().onLogin(data);
                }
            }

            @Override
            public void onFail(int code, String msg) {
                if (isViewAttached()) {
                    view().onFailed(code, msg);
                }
            }
        });
    }
}
