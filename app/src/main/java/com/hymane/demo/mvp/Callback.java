package com.hymane.demo.mvp;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/13
 * Description:测试回调
 */
public interface Callback<T> {
    void onSuccess(T data);

    void onFail(int code, String msg);

}
