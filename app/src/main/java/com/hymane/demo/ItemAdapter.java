package com.hymane.demo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

/**
 * Author   :hymane
 * Email    :hymanmee@gmail.com
 * Create at 2018/11/7
 * Description:
 */
public class ItemAdapter extends RecyclerView.Adapter {
    private Animation animation;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_anim_view, viewGroup, false);
        animation = AnimationUtils.loadAnimation(viewGroup.getContext(), R.anim.anim_round_rotate);
        animation.setInterpolator(new LinearInterpolator());
        return new ItemHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (animation != null) {
            ((ItemHolder) viewHolder).imageView.setAnimation(animation);
            ((ItemHolder) viewHolder).imageView.setLayerType(View.LAYER_TYPE_HARDWARE,null);
            animation.start();
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class ItemHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
